from math import log, sqrt
from decimal import Decimal
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max
from sasha.models import Word, Digram, Content, Cos
from utils import get_sentences, get_stem
from random import random


def persist_and_return_word(word, stem=None):
    if stem is None:
        stem = get_stem(word)
    try:
        word_ent = Word.objects.get(root=stem)
    except ObjectDoesNotExist:
        word_ent = Word(word=word, root=stem)
        word_ent.save()
    return word_ent


def persist_and_count_digram(word1, word2):
    try:
        digram_ent = Digram.objects.get(word1=word1, word2=word2)
        digram_ent.tf += 1
    except ObjectDoesNotExist:
        digram_ent = Digram(word1=word1, word2=word2, tf=1)
    digram_ent.save()
    return digram_ent


def cos_exists(word1, word2):
    return Cos.objects.filter(word1_id=word1.id, word2_id=word2.id).exists()


def process_sentence(sentence):
    words = sentence.split()
    if len(words) == 0:
        return
    prev_ent = persist_and_return_word(words[0])
    for word in words[1:]:
        curr_ent = persist_and_return_word(word)
        persist_and_count_digram(prev_ent, curr_ent)
        prev_ent = curr_ent


def get_all_words_and_digrams():
    content_set = Content.objects.all()
    cnt = 1
    for content in content_set.iterator():
        if content.text is not None:
            for sentence in get_sentences(content.text):
                process_sentence(sentence)
            print cnt
            cnt += 1


def calc_idfs():
    word_set = Word.objects.all()
    total = word_set.count()
    cnt = 1
    for word in word_set.iterator():
        appears_in = Digram.objects.filter(word2_id=word.id).count()
        word.idf = log(total/(1+appears_in))
        word.save()
        print cnt
        cnt += 1


def calc_tf_idfs():
    word_set = Word.objects.all()
    cnt = 1
    for word in word_set.iterator():
        digram_set = Digram.objects.filter(word1_id=word.id)
        max_f = digram_set.aggregate(Max('tf'))['tf__max']
        for digram in digram_set:
            digram.tfIdf = Decimal(digram.tf) / max_f * digram.word2.idf
            digram.save()
        print cnt
        cnt += 1


def calc_norms():
    word_set = Word.objects.all()
    cnt = 1
    for word in word_set.iterator():
        digram_set = Digram.objects.filter(word1_id=word.id)
        norm = Decimal(0)
        for digram in digram_set:
            norm += digram.tfIdf * digram.tfIdf
        word.norm = sqrt(norm)
        word.save()
        print cnt
        cnt += 1


def dot_product(v1_it, v2_it):
    product = Decimal(0)
    try:
        x1 = v1_it.next()
        x2 = v2_it.next()
        while True:
            if x1.word2.id == x2.word2.id:
                product += x1.tfIdf * x2.tfIdf
                x1 = v1_it.next()
                x2 = v1_it.next()
            elif x1.word2.id < x2.word2.id:
                x1 = v1_it.next()
            else:
                x2 = v2_it.next()
    except StopIteration:
        pass
    return product


def get_all_cos(word1, safe=True, exe_factor=None):
    total = Word.objects.count()
    if Cos.objects.filter(word1_id=word1.id).count() >= total - 1:
        return
    v1_set = [x for x in
              Digram.objects.filter(word1_id=word1.id).order_by('word2_id')]
    for word2 in  Word.objects.exclude(id__in = [d.word2_id for d in
            Digram.objects.filter(word1_id=word1.id)]).iterator():
        if word1.id != word2.id and (not safe or not cos_exists(word1, word2)):
            if exe_factor is not None and random() > exe_factor:
                continue
            if word1.norm != 0 and word2.norm != 0:
                cos = dot_product(
                        iter(v1_set),
                        Digram.objects.filter(word1_id=word2.id).order_by('word2_id').iterator()
                        ) / (word1.norm * word2.norm)
            else:
                cos = 0
            cos_ent = Cos(word1=word1, word2=word2, value=cos)
            cos_ent.save()
            if not cos_exists(word2, word1):
                cos_ent = Cos(word1=word2, word2=word1, value=cos)
                cos_ent.save()
