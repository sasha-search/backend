from django.conf.urls import patterns, url

from sasha import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^api/search.json', 'sasha.views.query_word', name='search'),
                       url(r'^api/search_reload.json', 'sasha.views.query_word_reload', name='search_reload'),
                       )
