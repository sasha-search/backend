from subprocess import Popen, PIPE
import os.path
import re

__DIR__ = os.path.dirname(__file__)

with open(os.path.join(__DIR__, 'stop-words_spanish_2_es.txt'), 'r') as _fo:
    REMOVE_LIST = []
    for word in _fo:
        REMOVE_LIST.append(word.strip())


_cache = {}


def get_stem(s):
    if s in _cache:
        return _cache[s]
    popen = Popen([os.path.join(__DIR__, 'stemmer'), s], stdout=PIPE)
    stdout, _ = popen.communicate()
    root = stdout.strip()
    _cache[s] = root
    return root


_stop_re = re.compile(r'(^|\b)('+('|'.join(REMOVE_LIST))+r')($|\b)',
        flags=re.IGNORECASE|re.UNICODE)
_non_alpha_re = re.compile(r'[\W\d_]+', flags=re.UNICODE)


def get_sentences(text):
    for paragraph in (text.split('/')):
        for sentence in paragraph.split('.'):
            yield _non_alpha_re.sub(' ', _stop_re.sub('', sentence).strip())
