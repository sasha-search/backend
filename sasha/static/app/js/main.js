(function(ang){
	
	var app = ang.module('app',[]);

	app.filter('formatDate',function(){
		return function(_date){
			var _d = new Date(_date);
			return ((_d.getDate().toString().length==1)?'0'+_d.getDate():_d.getDate())+'-'+
				(((_d.getMonth().toString()+1).length==1)?'0'+(_d.getMonth()+1):(_d.getMonth())+1)+'-'+
                //(_d.getMonth()+1)+'-'+
				'20'+_d.getYear().toString().substring(1,3);
		}
	})

	app.directive('body',function(){
		return {
			restrict : 'E',
			link : function(scope,elem){
				elem[0].onkeydown = function(e){
					if(e.keyCode==27){
						scope.hideSearchResults();
						scope.$apply();
					}
				}
			}
		}
	});

	app.directive('searchInput',function(){
		return {
			restrict : 'C',
			link : function(scope,elem){
				elem[0].onkeydown = function(e){
					if(e.keyCode==13){
						scope.search();
						scope.$apply();
					}
				}
			}
		}
	});

	app.factory('search',function($http){
		return function(params,callback){
            var _d = {
                to : (params.searchByDate?params.fromDate:(new Date())),
                from : (params.searchByDate?params.toDate:(new Date( Date.now()-(30*24*3600*1000) )))
            };
			$http.post('/api/search.json',{
				consulta : params.query,
                //fromDate : (new Date( Date.now()-(30*24*3600*1000) )),
				//toDate : (new Date()),
                fromDate : _d.from,
                toDate : _d.to,
				flag : (params.searchByDate?1:0),
				page : params.page||0
			}).success(function(_results){
				callback(_results);
			});
		};
	})

	app.controller('ui',function($scope,$http,$interval,$timeout,search){

		$scope.searchByDate = 0;
		$scope.currentPage;
		$scope.rType = 'rt2';
		var moveWordsInterval;

		$scope.search = function(){
			if(!$scope.query) return;
			$scope.searchActive = true;
			$scope.results = [];
			switch($scope.rType){
				case 'rt1':
					search({
						query : $scope.query,
						searchByDate : $scope.searchByDate,
		                fromDate : $scope.fromDate,
		                toDate : $scope.toDate,
					},function(_results){
						$scope.currentPage = 0;
						for(var i=0;i<_results.length;i++){
							$scope.results.push({
								title : _results[i].fields.title,
								text : _results[i].fields.text.substring(0,80),
								url : _results[i].fields.url,
								date : _results[i].fields.date
							});
						}
					});
				break;
				case 'rt2':
					var displayResults = function(_results) {
						// Loading results
							for(var i=0;i<_results.length;i++){
								$scope.results.push({
									word : _results[i].title,
									rank : _results[i].rank
								});
							}
						// Get max results rank
							var maxRank = $scope.results[0].rank;
							var wContainer = document.getElementsByClassName('words-results-container')[0];
							$scope.results.forEach(function(_r) {
								maxRank = Math.max( maxRank , _r.rank );
							});
						// Set random position and settings
							var counter = 0;
							$scope.results.forEach(function(_r) {
								_r.lPosition = Math.floor(Math.random()*wContainer.offsetWidth)-50;
								_r.tPosition = Math.floor(Math.random()*wContainer.offsetHeight)-20;
								_r.dX = ( (Math.floor(Math.random()*2))?1:-1 );
								_r.dY = ( (Math.floor(Math.random()*2))?1:-1 );
								_r.rank = _r.rank/maxRank;
								_r.fSize = Math.round(35*_r.rank);
								_r.buffRank = _r.rank;
								_r.rank = 0;
								$timeout(function() {
									_r.rank = 0.1 + (_r.buffRank * 0.9);
									$scope.$apply();
								},300*counter);
								counter++;
							});
							$interval.cancel(moveWordsInterval);
							moveWordsInterval = $interval(function(){
								$scope.results.forEach(function(_r) {
									_r.lPosition += _r.dX;
									_r.tPosition += _r.dY;
									_r.lPosition = _r.lPosition%(wContainer.offsetWidth-50);
									_r.tPosition = _r.tPosition%(wContainer.offsetHeight-20);
									if(_r.lPosition<0) _r.lPosition = Math.floor(Math.random()*wContainer.offsetWidth)-50;
									if(_r.tPosition<0) _r.tPosition = Math.floor(Math.random()*wContainer.offsetHeight)-20;
								});
							},100);
					}
					$http.post('/api/search_reload.json',{
						consulta : $scope.query,
						page : 1
					}).success(function(_resp){
            console.log(_resp);
						displayResults(_resp.results);
					});

				break;
			}
		}

		$scope.onClickWordEvent = function(_word) {
			$scope.query = _word;
			$scope.search();
			console.log($scope.query)
		}

		$scope.changePage = function(page){
			if(!$scope.query) return;
			$scope.results = [];
			search({
				query : $scope.query,
				searchByDate : $scope.searchByDate,
                fromDate : $scope.fromDate,
                toDate : $scope.toDate,
				page : page
			},function(_results){
				$scope.currentPage = page;
				for(var i=0;i<_results.length;i++){
					$scope.results.push({
						title : _results[i].fields.title,
						text : _results[i].fields.text.substring(0,80),
						url : _results[i].fields.url,
						date : _results[i].fields.date
					});
				}
			});	
		}

		$scope.hideSearchResults = function(){
			$scope.query = '';
			$scope.searchActive = false;
			$interval.cancel(moveWordsInterval);
		}

	});

})(angular);
