CREATE OR REPLACE FUNCTION query_word (indices integer[]) 
RETURNS SETOF content AS
$BODY$

BEGIN

RETURN QUERY

SELECT c.id, c.title, c.url, c.text, c.date FROM content c WHERE c.id = ANY(indices);

END;
$BODY$
language plpgsql;
