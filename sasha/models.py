from django.db import models


class Content(models.Model):
    title = models.CharField(max_length=250, blank=True)
    url = models.CharField(max_length=250)
    text = models.TextField(blank=True)
    date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'content'


class Word(models.Model):
    word = models.CharField(max_length=250, blank=False)
    root = models.CharField(max_length=250, blank=False, db_index=True)
    idf = models.DecimalField(null=True, decimal_places=9, max_digits=15)
    norm = models.DecimalField(null=True, decimal_places=9, max_digits=15)


class Digram(models.Model):
    word1 = models.ForeignKey(Word, db_index=True, verbose_name="Word 1 relation",related_name="digrams_first")
    word2 = models.ForeignKey(Word, db_index=True, verbose_name="Word 2 relation",related_name="digrams_second")
    tf = models.IntegerField(null=False,default=1)
    tfIdf = models.DecimalField(null=True,decimal_places=9,max_digits=15)

    class Meta:
        index_together = [
            ["word1", "word2"],
        ]


class Cos(models.Model):
    value = models.DecimalField(null=True,db_index=True,decimal_places=9,max_digits=15)
    word1 = models.ForeignKey(Word,db_index=False,verbose_name="Word 1 relation",related_name="CosWordsBefore")
    word2 = models.ForeignKey(Word,db_index=False,verbose_name="Word 2 relation",related_name="CosWordsAfter")

    class Meta:
        index_together = [
            ["word1", "word2"],
        ]
