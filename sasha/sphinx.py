import MySQLdb
import re
from datetime import datetime


class SashaIndex(object):
    non_alphanum = re.compile(ur'[\W_]+', flags=re.UNICODE)

    def __init__(self, host='127.0.0.1', port=9306):
        self.conn = MySQLdb.connect(host, port=port)

    @staticmethod
    def clear(s):
        return re.sub(SashaIndex.non_alphanum, u' ', s)

    def search(self, query, page=0):
        """
        :type page: int
        :type query: unicode, str
        :rtype: list
        """
        cur = self.conn.cursor()
        sql = "select id from sashaindex where match('%s') limit %d,%d" % \
            (self.clear(query), page * 10, 10)
        cur.execute(sql)
        return [x[0] for x in cur.fetchall()]

    def search_between_dates(self, query, from_date, to_date, page=0):
        """
        :type query: unicode, str
        :type from_date: datetime
        :type to_date: datetime
        :rtype: list
        """
        cur = self.conn.cursor()
        sql = "select id from sashaindex where match('%s') and date >= "\
            "%s and date <= %d order by date desc limit %d,%d" %\
              (self.clear(query), from_date.strftime("%s"),
               int(to_date.strftime("%s"))+24*3600, page * 10, 10)
        cur.execute(sql)
        return [x[0] for x in cur.fetchall()]


if __name__ == '__main__':
    idx = SashaIndex()
    print idx.search("alan_garcia)")
    print idx.search_between_dates("alan garcia", datetime(2014, 1, 1),
                                   datetime(2014, 8, 1))
