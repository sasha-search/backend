from django.contrib import admin
from sasha.models import Content


class ContentAdmin(admin.ModelAdmin):
    list_display = ('title', 'url')


admin.site.register(Content, ContentAdmin)
