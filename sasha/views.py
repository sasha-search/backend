from django.shortcuts import render
from django.http import HttpResponse
from models import Content
from models import Word
from models import Cos
from sasha.sphinx import SashaIndex
from sasha.utils import get_stem
from sasha.processing import *
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator
import json


def index(request):
    return render(request, 'index.html')


@csrf_exempt
def query_word(request):
    if request.method == 'POST': # If the form has been submitted...
        #valores que se tienen que recibir como peticion

        post = json.loads(request.body)
        print 'CONSULTA', post
        str_query = post['consulta']
        str_from_date = post['fromDate']
        str_to_date = post['toDate']
        bool_flag = post['flag']
        try:
            int_number_page = int(post['page'])
        except ValueError:
            int_number_page = 0

        sashaindex = SashaIndex()

        if bool_flag == 0:
            indices = sashaindex.search(str_query, int_number_page)
        else:
            date_from_date = datetime.strptime(str_from_date[:10], "%Y-%m-%d")
            date_to_date = datetime.strptime(str_to_date[:10], "%Y-%m-%d")

            indices = sashaindex.search_between_dates(str_query, date_from_date,
                                                      date_to_date,
                                                      int_number_page)
        results = Content.objects.raw('SELECT * FROM query_word(%s)', [indices])
        return HttpResponse(serializers.serialize('json', results),
                            content_type="application/json")

    else:
        return HttpResponse("-1")

@csrf_exempt
def query_word_reload (request):
    if request.method == 'POST': # If the form has been submitted...
        #valores que se tienen que recibir como peticion
        
        post = json.loads(request.body)
        str_query = post['consulta']
        try:
            int_number_page = int(post['page'])
        except ValueError:
            int_number_page = 1

        root_word = get_stem(str_query)

        result_root = Word.objects.filter(root=root_word)

        # creating json object
        response_data = {}
        response_result = []
        response_data['results'] = response_result
        
        if not result_root :
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        else :

            per_root = persist_and_return_word (str_query, root_word)
            
            get_all_cos(per_root, exe_factor=0.01)

            results = Cos.objects.filter(word1=per_root).select_related('word2').order_by('-value')

            paginator = Paginator (results, 40) #Limit of registers to show

            page = paginator.page(int_number_page)
            
            values = [{'title': i.word2.word, 'rank': str(i.value)} for i in page]

            response_data['results'] = values
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        
    else :
        return HttpResponse("-1")
